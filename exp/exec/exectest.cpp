#include <iostream>
#include <wx/app.h>
#include <wx/utils.h>

#ifdef _WIN32
#define EXEEXT ".exe"
#include <fstream>
#else
#define EXEEXT ""
#endif

using namespace std;

// Define a new application type, each program should derive a class from wxApp
class MyApp : public wxApp
{
public:
    // override base class virtuals
    // ----------------------------

    // this one is called on application startup and is a good place for the app
    // initialization (doing it here and not in the ctor allows to have an error
    // return: if OnInit() returns false, the application terminates)
    virtual bool OnInit();
};

IMPLEMENT_APP(MyApp)

void test(int flag)
{
  int code;

  cout << "* Good, returns immediately" << endl;
  code = ::wxExecute(_T("./sleep"EXEEXT" --version"), flag);
  cout << dec << code << " - " << hex << code << endl;

  cout << "* Not found (with params -> subshell)" << endl;
  code = ::wxExecute(_T("./sleepe"EXEEXT" 1 --version"), flag);
  cout << dec << code << " - " << hex << code << endl;

  cout << "* Not found (no params -> no subshell?)" << endl;
  code = ::wxExecute(_T("sleepe"EXEEXT), flag);
  cout << dec << code << " - " << hex << code << endl;

  cout << "* Bad option, returns 1" << endl;
  code = ::wxExecute(_T("./sleep"EXEEXT" --versione"), flag);
  cout << dec << code << " - " << hex << code << endl;

  cout << "* Good, sleeps 1 second" << endl;
  code = ::wxExecute(_T("./sleep"EXEEXT" 1"), flag);
  cout << dec << code << " - " << hex << code << endl;

  cout << "* Segfault (bash $?=139)" << endl;
  //code = ::wxExecute(_T("./segfault"EXEEXT), flag);
  cout << dec << code << " - " << hex << code << endl;

  cout << "* exit(EXIT_FAILURE==1)" << endl;
  code = ::wxExecute(_T("./exit_failure"EXEEXT), flag);
  cout << dec << code << " - " << hex << code << endl;
}

bool MyApp::OnInit()
{
#ifdef _WIN32
    std::streambuf* cout_sbuf = std::cout.rdbuf();
    std::ofstream   fout("stdout.txt");
    std::cout.rdbuf(fout.rdbuf());
#endif
  cout << "SYNC" << endl;
  test(wxEXEC_SYNC);
  cout << endl;
  cout << "ASYNC" << endl;
  test(wxEXEC_ASYNC);

  return false;
}
