CXXFLAGS=$(wx-config --cflags) LDLIBS=$(wx-config --libs) make exectest
i586-mingw32msvc-g++ \
  $(/usr/local/i586-mingw32msvc/bin/wx-config --cflags) \
  exectest.cpp -o exectest.exe \
  $(/usr/local/i586-mingw32msvc/bin/wx-config --libs)
for i in segfault exit_failure false sleep; do
    make $i
    i586-mingw32msvc-gcc $i.c -o $i.exe
done
