#include <stdio.h>
#include <locale.h>
#include <libintl.h>

int main(void)
{
  setlocale(LC_MESSAGES, "");
  bindtextdomain("dfarc", "/usr/local/share/locale");
  textdomain("dfarc");
  printf("Play->%s\n", gettext("Play"));
}
