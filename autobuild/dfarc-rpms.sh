#!/bin/bash
# Fedora release

# Copyright (C) 2008  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

SRC_PACKAGE=dfarc
BIN_PACKAGE=freedink-dfarc
VERSION=$1
if [ -z "$VERSION" ]; then
    TARBALL=$(cd /mnt/snapshots && ls $SRC_PACKAGE-*.tar.gz | sort -n | tail -1)
    VERSION=${TARBALL#$SRC_PACKAGE-} # remove prefix
    VERSION=${VERSION%.tar.gz}   # remove suffix
else
    TARBALL="$SRC_PACKAGE-$VERSION.tar.gz"
fi

SPECFILE=$BIN_PACKAGE.spec
cd /usr/src
cp -a /mnt/snapshots/$TARBALL rpmbuild/SOURCES/
cp $SRC_PACKAGE/$SPECFILE rpmbuild/SPECS/
sed -i -e "s/^Version:.*/Version:	$VERSION/"  rpmbuild/SPECS/$SPECFILE
rpmbuild -bs --nodeps rpmbuild/SPECS/$SPECFILE
mock -r fedora-10-i386 --resultdir /mnt/snapshots/fedora --rebuild rpmbuild/SRPMS/$BIN_PACKAGE-$VERSION-1*.src.rpm
cp -f rpmbuild/SPECS/$SPECFILE /mnt/snapshots/fedora
make -C /mnt/snapshots/fedora/
