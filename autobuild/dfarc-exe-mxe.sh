#!/bin/bash -ex
# MS Woe release

# Copyright (C) 2008, 2014, 2017  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# cd /usr/src/mxe/  # master branch
## Use MinGW-w64 in /usr/src/mxe/settings.mk: MXE_TARGETS := x86_64-w64-mingw32.static i686-w64-mingw32.static
# make -j2 JOBS=$(nproc) bzip2 wxwidgets
#               ^^ adapt to available RAM!
# 3.12: using MXE master 1389227aad9ae57de2296fff64e31eeff2693848

PACKAGE=dfarc
VERSION=$(make version | tail -1)

make dist
rm -rf $PACKAGE-$VERSION/
tar xzf $PACKAGE-$VERSION.tar.gz
pushd $PACKAGE-$VERSION/
mkdir zip/

# documentation
for i in README.txt TRANSLATIONS.txt; do
    cp $i zip/dfarc-$i
done
for i in AUTHORS COPYING NEWS THANKS; do
    cp $i zip/dfarc-$i.txt
done
cat <<EOF > zip/dfarc-SOURCE.txt
The DFArc source code is available at:
  http://ftp.gnu.org/gnu/freedink/

The source code is the "recipe" of DFArc, that let you study it,
modify it, and redistribute your changes.  The GNU GPL license
explicitely allows you to do so (see dfarc-COPYING.txt).

If you upload a DFArc .exe on your website, you must also offer the
corresponding source code for download.
EOF

# Include documentation with MS-DOS newlines (if not already)
sed -i -e 's/\(^\|[^\r]\)$/\1\r/' zip/dfarc-*.txt


##
# Compile!
##
# full-static
PATH=/usr/src/mxe/usr/bin:$PATH

rm -rf cross-woe-32/
mkdir cross-woe-32/
pushd cross-woe-32/
CXXFLAGS='-std=c++11' ../configure --host=i686-w64-mingw32.static \
  --with-wx-config=/usr/src/mxe/usr/i686-w64-mingw32.static/bin/wx-config \
  --enable-upx
make -j$(nproc) install-strip DESTDIR=`pwd`/destdir
# move .exe but avoid symlinks
find destdir/usr/local/bin/ -type f -name "*.exe" | while read file; do
  mv $file ../zip/
done
mv destdir/usr/local/lib/locale ../zip/po
popd
#rm -rf cross-woe-32/

rm -rf cross-woe-64/
mkdir cross-woe-64/
pushd cross-woe-64/
../configure --host=x86_64-w64-mingw32.static \
  --with-wx-config=/usr/src/mxe/usr/x86_64-w64-mingw32.static/bin/wx-config \
  --enable-upx
make -j$(nproc) install-strip DESTDIR=`pwd`/destdir
# move .exe but avoid symlinks
find destdir/usr/local/bin/ -type f -name "*.exe" | while read file; do
  mv $file ../zip/$(basename ${file%.exe}64.exe)
done
popd
#rm -rf cross-woe-64/

(cd zip/ && zip -r ../../$PACKAGE-$VERSION-bin.zip .)


popd
#rm -rf $PACKAGE-$VERSION/
