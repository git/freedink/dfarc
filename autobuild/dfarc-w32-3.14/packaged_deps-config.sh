#!/bin/bash -ex
# Packaged build dependencies - initial configuration

# Copyright (C) 2017, 2018  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Configuration
echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/00InstallRecommends

# Use a snapshot.debian.org sources.list. More reproducible but will
# miss security updates on rebuild, so update if necessary.

# Note: don't use backports, versions may change at any time

cat <<'EOF' > /etc/apt/sources.list
deb http://snapshot.debian.org/archive/debian/20180530T101715Z/ stretch main
deb http://snapshot.debian.org/archive/debian-security/20180530T132422Z/ stretch/updates main
EOF

apt-get update
