#!/bin/bash -ex
# Build MXE environment

# Copyright (C) 2017, 2018  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

cd /opt/
git clone -n https://github.com/mxe/mxe.git
cd mxe/
git reset --hard 5d4c388be33414e7a802c4959d3d22f759840587

# Reproducible build
# MXE has patched binutils with linker SOURCE_DATE_EPOCH support
# This should trigger some reproducible support
export SOURCE_DATE_EPOCH=$(cd /opt/mxe && git log --format="%ct" -n 1)

make -j2 JOBS=$(nproc) gcc
# Reproducible build
# Fixup libstdc++.a ordering; would require stable ordering in the GCC build system
(
    cd /opt/mxe/usr/lib/gcc/i686-w64-mingw32.static/5.5.0/
    mkdir t && cd t/
    ar x ../libstdc++.a
    rm -f ../libstdc++.a
    ar Dvcr ../libstdc++.a $(ls | LC_ALL=C sort)
    cd ..
    rm -rf t/
)

# Reproducible build
# Manually timestamp wxWidgets, waiting for compiler SOURCE_DATE_EPOCH support (probably in gcc6...)
cat <<'EOF' > /opt/mxe/src/wxwidgets-2-reproducible.patch
diff -ru wxWidgets-3.0.2/src/common/utilscmn.cpp wxWidgets-3.0.2-reproducible/src/common/utilscmn.cpp
--- wxWidgets-3.0.2/src/common/utilscmn.cpp	2014-10-06 23:33:44.000000000 +0200
+++ wxWidgets-3.0.2-reproducible/src/common/utilscmn.cpp	2018-05-30 22:49:03.016608743 +0200
@@ -1404,8 +1404,8 @@
                "none",
 #endif
                wxDEBUG_LEVEL,
-               __TDATE__,
-               __TTIME__,
+               "May 28 2018",
+               "05:41:12",
                wxPlatformInfo::Get().GetToolkitMajorVersion(),
                wxPlatformInfo::Get().GetToolkitMinorVersion()
               );
EOF

make -j2 JOBS=$(nproc) bzip2 wxwidgets
