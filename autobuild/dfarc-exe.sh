#!/bin/bash -ex
# MS Woe release

# Copyright (C) 2008  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

PACKAGE=dfarc
VERSION=$1
if [ -z "$VERSION" ]; then
    VERSION=$(cd /mnt/snapshots/$PACKAGE && ls -d */ | sed 's,/$,,' | sort -n | tail -1)
fi

PUBDIR=/mnt/snapshots/$PACKAGE/$VERSION
PUBDIR_WOE=$PUBDIR/woe
mkdir -p $PUBDIR_WOE

rm -rf t/
mkdir t
pushd t
tar xzf $PUBDIR/$PACKAGE-$VERSION.tar.gz
cd $PACKAGE-$VERSION/
mkdir zip

# DLLs
for i in wxbase28_gcc_custom.dll wxmsw28_core_gcc_custom.dll; do
   cp -a /usr/local/i586-mingw32msvc/lib/$i zip/
done
for i in libbz2-1.dll; do
   cp -a /usr/local/i586-mingw32msvc/bin/$i zip/
done
zcat /usr/share/doc/mingw32-runtime/mingwm10.dll.gz > zip/mingwm10.dll
i586-mingw32msvc-strip zip/mingwm10.dll
upx zip/*.dll

# documentation
for i in README.txt TRANSLATIONS.txt; do
    cp $i zip/dfarc-$i
done
for i in AUTHORS COPYING NEWS THANKS; do
    cp $i zip/dfarc-$i.txt
done
cat <<EOF > zip/dfarc-SOURCE.txt
The DFArc source code is available at:
  http://ftp.gnu.org/gnu/freedink/
  http://www.freedink.org/snapshots/

The source code is the "recipe" of DFArc, that let you study it,
modify it, and redistribute your changes.  The GNU GPL license
explicitely allows you to do so (see dfarc-COPYING.txt).

If you upload a DFArc .exe on your website, you must also offer the
corresponding source code for download.
EOF

cat <<EOF > zip/dfarc-DLL.txt
The .dll files are compiled versions of several free software
projects.

See
  http://www.freedink.org/releases/woe/depsources/
  http://www.freedink.org/snapshots/woe/depsources/
for their source code, and
  doc/cross.txt
in the DFArc source code to see how they were compiled.

See also licenses/ for your rights on these projects.
EOF

mkdir -m 755 zip/licenses
# TODO: the cross-compilation env should provide for each dependency
# - current version
# - source tarball 
# - licenses
#   - subdependency licenses
cp COPYING zip/licenses/${PACKAGE}-${VERSION}_COPYING
cp /usr/src/bzip2-1.0.6/LICENSE zip/licenses/bzip2-1.0.6_LICENSE
cp /usr/src/wxWidgets-2.8.11/docs/licence.txt zip/licenses/wxWidgets-2.8.11_licence.txt
cp /usr/src/wxWidgets-2.8.11/docs/lgpl.txt zip/licenses/wxWidgets-2.8.11_lgpl.txt
cp /usr/share/doc/mingw32-runtime/copyright zip/licenses/mingw32-3.13-runtime_copyright

# Keep track of dependencies for _each_ build, since dependencies
# versions may differ
mkdir -p $PUBDIR_WOE/depsources/
for i in wxWidgets-2.8.11.tar.bz2 bzip2-1.0.6.tar.gz mingw32-runtime_3.13*; do
    ln -f /mnt/snapshots/woe/depsources/$i $PUBDIR_WOE/depsources/
done

# Include documentation with MS-DOS newlines (if not already)
for i in dfarc-*.txt licenses/*; do
    sed -i -e 's/\(^\|[^\r]\)$/\1\r/' zip/dfarc-*.txt zip/licenses/*
done


##
# Compile!
##
# .dll
# wxWidgets uses different CFLAGS on shared mode, so we need to
# recompile everything
mkdir cross
pushd cross/
CPPFLAGS="-I /usr/local/i586-mingw32msvc/include" \
  LDFLAGS="-L /usr/local/i586-mingw32msvc/lib" \
  ../configure --host=i586-mingw32msvc --build=i686-pc-linux-gnu \
  --with-wx-config=/usr/local/i586-mingw32msvc/bin/wx-config \
  --enable-upx
make install-strip DESTDIR=`pwd`/destdir
find destdir/usr/local/bin/ -type f -name "*.exe" | while read file; do
  cp -a $file $PUBDIR_WOE/$(basename ${file%.exe}-$VERSION-dll.exe)
  mv $file ../zip/
done
# gettext translation files:
cp -a destdir/usr/local/lib/locale ../zip/po
# translation template:
cp ../po/dfarc.pot ../zip/po/
popd
rm -rf cross/

# Publish
rm -f $PUBDIR_WOE/$PACKAGE-$VERSION-bin.zip
(cd zip/ && zip -r $PUBDIR_WOE/$PACKAGE-$VERSION-bin.zip *)


##
# Compile! (2)
##
# full-static
mkdir cross
pushd cross/
CPPFLAGS="-I /usr/local/i586-mingw32msvc/include" \
  LDFLAGS="-L/usr/local/i586-mingw32msvc/lib" \
  ../configure --host=i586-mingw32msvc --build=i686-pc-linux-gnu \
  --with-wx-config=/usr/local/i586-mingw32msvc/bin/wx-config \
  --enable-static --enable-upx
make install-strip DESTDIR=`pwd`/destdir
# move .exe but avoid symlinks
find destdir/usr/local/bin/ -type f -name "*.exe" | while read file; do
  mv $file $PUBDIR_WOE/$(basename ${file%.exe}-$VERSION.exe)
done
popd
rm -rf cross/


popd
#rm -rf t
